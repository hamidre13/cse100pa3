/*
 Name:Seyedhamidreza Tavakolifard
 Account:stavakol
 Pid:A11489441
 */

#ifndef __CSE100PA3__BitOutputStream__
#define __CSE100PA3__BitOutputStream__

#include <iostream>
#include <string>

class BitOutputStream
{
public:
    BitOutputStream(std::ostream &out);
    ~BitOutputStream();
    
    // pack a single bit
    void writeBit(bool bit);
    
    // send the byte to the stream
    void flush();
    
    std::ostream &out;
    
private:
    int count;
    char byte;
};
#endif /* defined(__CSE100PA3__BitOutputStream__) */
