/*
 Name:Seyedhamidreza Tavakolifard
 Account:stavakol
 Pid:A11489441
 */
#include "HCTree.hpp"
#include <algorithm>
#include <bitset>
#include <vector>



HCTree::~HCTree()
{
    if (root) {
        clear(root);
    }
    else
        cout<<"This is an empty tree!!";
}

//Using recursive function to delete the huffman code tree
void HCTree::clear (HCNode *node)
{
    if (node->c0) {
        clear(node->c0);
    }
    if (node->c1) {
        clear(node->c1);
    }
    delete node;
}
void HCTree::build(const vector<int>& freqs)
{
    cout<<"Start building Huffman tree "<<endl;
    
    /*
     1-iterate thorugh the freqs
     2- Store them in the prioroty quee
     3- Make the tree
     */

	//Iterate through freq and push them in priority queue
	//priority queue will orgnize the nodes based on their reaping time
    for (int i= 0; i<freqs.size(); i++) {			
        HCNode* node = new HCNode(freqs[i],i);
        leaves[i] = node;
        if (leaves[i]) {
            pq.push(leaves[i]);
        }
        
    }

	// Since the least important onec are at the top of pq we follow the huffman algoritme to build the tree from buttom up.
    while (pq.size()>1) {
        HCNode *first = pq.top();
        pq.pop();
        HCNode *second = pq.top();
        pq.pop();
        int freqcount = first->count+second->count;
        HCNode *parent = new HCNode(freqcount,-1);
        parent->c0  = first;
        parent->c1 = second;
        pq.push(parent);
        
    }
    if (pq.size()) {
        root = pq.top();
        cout<<"tree is ready"<<endl;
    }
    else
        cout<<"Empty tree...."<<endl;
        
}
void HCTree::encode(byte symbol, BitOutputStream& out) const
{
	cout<<"start encoding";
    HCNode *curr  =leaves[symbol];
    vector <int> codedsymb;
    
    while (curr->p) {
        int zeroOrOne;
        if (curr->c1) {
            zeroOrOne = 1;
        }
        else
            zeroOrOne =0;
        codedsymb.push_back(zeroOrOne);
    }
    
    while (codedsymb.size()>1) {
        out.writeBit(codedsymb.back());
	cout<<"the last elemnt is:"+codedsymb.back()<<endl;
        codedsymb.pop_back();
	cout<<"now the last element is:"+codedsymb.back()<<endl;
    }
    
    
}
int HCTree::decode(BitInputStream& in) const
{
    /*
     1-Read header file?
     2- construct the tree 
     3-Start from the root and triverse to get the 
     to the intended node and print out the symbele
     */
    HCNode * decoderoot = root;
    
    int bit;
    
    
    // traverse the tree until we run out of bits or get a leaf
	while (decoderoot->c0 || decoderoot->c1) {
		bit = in.next();
		if (bit < 0) {
			return -1;
		}
		if (bit) {
			decoderoot = decoderoot->c1;
		} else {
			decoderoot = decoderoot->c0;
		}
	}
	return decoderoot->symbol;
    
}
std::string HCTree::getCode(byte symbol) const {
	// used the cached bit sequence
	if (codes[symbol].size()) {
		return codes[symbol];
	}
    
	std::string code;
	HCNode* node = leaves[symbol];
    
	// follow the leaf up to the root
	while (node->p) {
		if (node->p->c0 && node == node->p->c0) {
			code = "0" + code;
		} else {
			code = "1" + code;
		}
		node = node->p;
	}
	return code;
}

/*
 Discussion for the PA3:
 Properties od Huffman trees:
 1- Binary
 2- Full = inrnal nodes have 2children
 3- N leaf nodes
 4- N-1 intrnal nodes
 
 
 Tip:Leaf level sequence of a full binary tree uniqualy determine the structure of the tree.
 Pair a symbol wit a leaf level (left to right or right to left) to get a uniqe Huffman tree.
 
 
 Compressing headers:
 leaf level Sequence
 
 
 Two Arrays method:
 
 
 
 Reading and writng in the files:
 
 ostream.write(char * int)
 istream (char * int)
 
 
 */



