/*
 Name:Seyedhamidreza Tavakolifard
 Account:stavakol
 Pid:A11489441
 */

#ifndef __CSE100PA3__BitInputStream__
#define __CSE100PA3__BitInputStream__

#include <iostream>
#include <string>
#include <vector>
#include <bitset>
using namespace std;
class BitInputStream
{
    
public:

    BitInputStream(istream &in);
    int next();
    void print();
    bool good;
    int size;
    std::vector<int> freqs;
    
private:
    // Get the freq vector at the header
    void getFreqs();
    
    // read the header used in compressed files
    void readHeader();
    
    // read a string into the bits vector
    void readBuffer(std::string buffer);
    
    int index;
    std::vector<bool> bits;
    std::string header;
    std::istream &in;
    
};

#endif /* defined(__CSE100PA3__BitInputStream__) */
