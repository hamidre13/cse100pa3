/*
 Name:Seyedhamidreza Tavakolifard
 Account:stavakol
 Pid:A11489441
 */

#include "HCNode.hpp"

/*
 Overrithing prioroty queue operator 
 in order to put them in it
 */
bool HCNode::operator<(const HCNode& other)
{
    if (count !=other.count) {
        return count >other.count;
    }
    return symbol <other.symbol;
}

bool comp(HCNode* one, HCNode* other)
{
    return one<other;
}